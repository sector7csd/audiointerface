// own header file
#include "de_sector7csd_jni_audiointerface_AudioInterface.h"

// c++ headers
#include <cstdio>
#include <string>
#include <map>

// fmod sound api
#include "fmod.h"

// platform specific includes - for loading SharedLib's
#ifdef __LINUX__
  #include <dlfcn.h>
#elif _WIN32
  #include <windows.h>
#endif // WIN32

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// Global stuff
#ifdef __LINUX__

#define SharedLibHandleType   void *
#define LoadSharedLib         dlopen
#define GetFunctionPointer    dlsym
#define FreeSharedLib         dlclose

#elif _WIN32

#define SharedLibHandleType   HINSTANCE
#define LoadSharedLib         LoadLibrary
#define GetFunctionPointer    GetProcAddress
#define FreeSharedLib         FreeLibrary

#endif // WIN32

static SharedLibHandleType                        lib_handle  = NULL;

static int																				nextSoundId = 0;
static FMOD_SYSTEM *  														fmodSystem	= NULL;

typedef std::map<int, FMOD_SOUND *> 							FSoundMap;
typedef std::map<int, FMOD_SOUND *>::iterator 		FSoundMapIt;

static FSoundMap  																sounds;

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

// declare function pointers
typedef FMOD_RESULT (*fp_FMOD_System_Create)			(FMOD_SYSTEM **);
typedef FMOD_RESULT (*fp_FMOD_System_Release)			(FMOD_SYSTEM *system);
typedef FMOD_RESULT (*fp_FMOD_System_Init)  			(FMOD_SYSTEM *system, int maxchannels, FMOD_INITFLAGS flags, void *extradriverdata);
typedef FMOD_RESULT (*fp_FMOD_System_CreateSound)	(FMOD_SYSTEM *system, const char *name_or_data, FMOD_MODE mode, FMOD_CREATESOUNDEXINFO *exinfo, FMOD_SOUND **sound);
typedef FMOD_RESULT (*fp_FMOD_System_PlaySound)		(FMOD_SYSTEM *system, FMOD_CHANNELINDEX channelid, FMOD_SOUND *sound, FMOD_BOOL paused, FMOD_CHANNEL **channel);
typedef FMOD_RESULT (*fp_FMOD_Sound_Release)			(FMOD_SOUND  *sound);

fp_FMOD_System_Create 			pFMOD_System_Create 			= NULL;
fp_FMOD_System_Release			pFMOD_System_Release			=	NULL;
fp_FMOD_System_Init					pFMOD_System_Init					= NULL;
fp_FMOD_System_CreateSound	pFMOD_System_CreateSound	= NULL;
fp_FMOD_System_PlaySound		pFMOD_System_PlaySound		= NULL;
fp_FMOD_Sound_Release				pFMOD_Sound_Release				= NULL;

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

void GetJStringContent(JNIEnv *AEnv, jstring AStr, std::string &ARes)
{
  if (!AStr)
  {
    ARes.clear();
    return;
  }

  const char *s = AEnv->GetStringUTFChars(AStr,NULL);
  ARes=s;
  AEnv->ReleaseStringUTFChars(AStr,s);
}

// --------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

/*
 * Class:     de_sector7csd_jni_audiointerface_AudioInterface
 * Method:    Init
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_de_sector7csd_jni_audiointerface_AudioInterface_init(JNIEnv * pEnv, jobject obj, jint channels)
{
	if(lib_handle == NULL)
	{
#ifdef __LINUX__
	#ifdef VERSION_64
		lib_handle = LoadSharedLib("./shared/libfmodex64.so", RTLD_LAZY);
	#else
    lib_handle = LoadSharedLib("./shared/libfmodex32.so", RTLD_LAZY);
	#endif // VERSION_64
#elif _WIN64
    lib_handle = LoadSharedLib(".\\shared\\fmodex64.dll");
#elif _WIN32
    lib_handle = LoadSharedLib(".\\shared\\fmodex.dll");
		
		#pragma comment(linker, "/EXPORT:Java_de_sector7csd_jni_audiointerface_AudioInterface_freeSound=_Java_de_sector7csd_jni_audiointerface_AudioInterface_freeSound@12")
		#pragma comment(linker, "/EXPORT:Java_de_sector7csd_jni_audiointerface_AudioInterface_init=_Java_de_sector7csd_jni_audiointerface_AudioInterface_init@12")
		#pragma comment(linker, "/EXPORT:Java_de_sector7csd_jni_audiointerface_AudioInterface_loadSound=_Java_de_sector7csd_jni_audiointerface_AudioInterface_loadSound@16")
		#pragma comment(linker, "/EXPORT:Java_de_sector7csd_jni_audiointerface_AudioInterface_playSound=_Java_de_sector7csd_jni_audiointerface_AudioInterface_playSound@12")
		#pragma comment(linker, "/EXPORT:Java_de_sector7csd_jni_audiointerface_AudioInterface_release=_Java_de_sector7csd_jni_audiointerface_AudioInterface_release@8")	
		
#endif
		if(lib_handle)
		{
			pFMOD_System_Create 			= reinterpret_cast<fp_FMOD_System_Create>			(GetFunctionPointer(lib_handle, "FMOD_System_Create"));
			pFMOD_System_Release			=	reinterpret_cast<fp_FMOD_System_Release>		(GetFunctionPointer(lib_handle, "FMOD_System_Release"));
			pFMOD_System_Init					= reinterpret_cast<fp_FMOD_System_Init>				(GetFunctionPointer(lib_handle, "FMOD_System_Init"));
			pFMOD_System_CreateSound	= reinterpret_cast<fp_FMOD_System_CreateSound>(GetFunctionPointer(lib_handle, "FMOD_System_CreateSound"));
			pFMOD_System_PlaySound		= reinterpret_cast<fp_FMOD_System_PlaySound>	(GetFunctionPointer(lib_handle, "FMOD_System_PlaySound"));
			pFMOD_Sound_Release				= reinterpret_cast<fp_FMOD_Sound_Release>			(GetFunctionPointer(lib_handle, "FMOD_Sound_Release"));

			if(pFMOD_System_Create 			&& pFMOD_System_Release 	&& pFMOD_System_Init &&
				 pFMOD_System_CreateSound && pFMOD_System_PlaySound && pFMOD_Sound_Release)
			{
				FMOD_RESULT res = pFMOD_System_Create(&fmodSystem);
				if(res != FMOD_OK)
				{
					printf("- pFMOD_System_Create failed\n");
					return -1;
				}

				res = pFMOD_System_Init(fmodSystem, channels, FMOD_INIT_NORMAL, 0);
				if(res != FMOD_OK)
				{
					printf("- pFMOD_System_Init failed\n");
					return -1;
				}

				printf("- FMOD ready for action\n");

			}
			else
			{
				printf("- at least one of function pointers is invalid\n");
				return -1;
			}
		}
		else
		{
			printf("- loading of FMOD shared lib failed\n");
			return -1;
		}
	}

	return 1;
}

/*
 * Class:     de_sector7csd_jni_audiointerface_AudioInterface
 * Method:    Release
 * Signature: ()I
 */
JNIEXPORT jint JNICALL Java_de_sector7csd_jni_audiointerface_AudioInterface_release(JNIEnv * pEnv, jobject obj)
{
	if(fmodSystem && pFMOD_System_Release)
	{
		FMOD_RESULT res = pFMOD_System_Release(fmodSystem);
		if(res != FMOD_OK)
		{
			printf("- pFMOD_System_Release failed\n");
			return -1;
		}

		fmodSystem = NULL;

		printf("- FMOD shutdown complete\n");

		FreeSharedLib(lib_handle);

		pFMOD_System_Create 			= NULL;
		pFMOD_System_Release			=	NULL;
		pFMOD_System_Init					= NULL;
		pFMOD_System_CreateSound	= NULL;
		pFMOD_System_PlaySound		= NULL;
		pFMOD_Sound_Release				= NULL;
	}

	return 1;
}

/*
 * Class:     de_sector7csd_jni_audiointerface_AudioInterface
 * Method:    LoadSound
 * Signature: (Ljava/lang/String;)I
 */
JNIEXPORT jint JNICALL Java_de_sector7csd_jni_audiointerface_AudioInterface_loadSound(JNIEnv * pEnv, jobject obj, jstring filename, jboolean stream)
{
	std::string strFileName;
	GetJStringContent(pEnv, filename, strFileName);
	printf("- loadsound with: %s\n", strFileName.c_str());

	if(fmodSystem && pFMOD_System_CreateSound)
	{
		FMOD_SOUND * snd;

		int flags = FMOD_DEFAULT;
		if(stream == true)
		{
			printf("- using streaming flag\n");

			flags |= FMOD_CREATESTREAM;
		}

		FMOD_RESULT res = pFMOD_System_CreateSound(fmodSystem, strFileName.c_str(), flags, NULL, &snd);
		if(res == FMOD_OK)
		{
			nextSoundId++;
			sounds[nextSoundId] = snd;
			return nextSoundId;
		}
	}

	return -1;
}

/*
 * Class:     de_sector7csd_jni_audiointerface_AudioInterface
 * Method:    PlaySound
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_de_sector7csd_jni_audiointerface_AudioInterface_playSound(JNIEnv * pEnv, jobject obj, jint sound)
{
	if(fmodSystem && pFMOD_System_PlaySound)
	{
		FSoundMapIt it = sounds.find(sound);
		if(it != sounds.end())
		{
			FMOD_SOUND * snd = it->second;
			FMOD_RESULT res = pFMOD_System_PlaySound(fmodSystem, FMOD_CHANNEL_FREE, snd, false, NULL);
			if(res == FMOD_OK)
			{
				return 1;
			}
		}
	}

	return -1;
}

/*
 * Class:     de_sector7csd_jni_audiointerface_AudioInterface
 * Method:    FreeSound
 * Signature: (I)I
 */
JNIEXPORT jint JNICALL Java_de_sector7csd_jni_audiointerface_AudioInterface_freeSound(JNIEnv * pEnv, jobject obj, jint sound)
{
	if(fmodSystem && pFMOD_Sound_Release)
	{
		FSoundMapIt it = sounds.find(sound);
		if(it != sounds.end())
		{
			FMOD_SOUND * snd = it->second;
			FMOD_RESULT res = pFMOD_Sound_Release(snd);
			if(res == FMOD_OK)
			{
				sounds.erase(it);
				return 1;
			}
		}
	}

	return -1;
}
