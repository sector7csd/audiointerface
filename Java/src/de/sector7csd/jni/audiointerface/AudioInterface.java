package de.sector7csd.jni.audiointerface;

import java.util.logging.Logger;

public class AudioInterface
{
	private static Logger T = Logger.getLogger(AudioInterface.class.getName());
		
	static
	{
		boolean is64Bit = System.getProperty("os.arch").equals("amd64") || 
						  System.getProperty("os.arch").equals("x86_64");
		
		String libName = "AudioInterface";
		if(is64Bit)
		{
			libName = libName + "64";
		}
		else
		{
			libName = libName + "32";
		}
		
		T.info("- loadLibrary: "+libName);
		try
		{
			System.loadLibrary(libName);
			T.info("- loadLibrary successful");
		}
		catch(Exception e)
		{
			T.warning("loadLibrary failed with: " + e);
		}
	}
		
	public native int 	init		(int channels);
	public native int 	release		();
	public native int	loadSound	(String fileName, boolean streaming);
	public native int	playSound	(int sound);
	public native int	freeSound	(int sound);
}
